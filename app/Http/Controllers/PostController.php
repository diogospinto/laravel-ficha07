<?php

namespace App\Http\Controllers;

use App\Post;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailLikedPhoto;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'Desc')->with('User')->paginate(5);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('photo')) {
            $fileName = $request->file('photo')->getClientOriginalName();
            $fileExtension = $request->file('photo')->getClientOriginalExtension();

            $image = md5($fileName) . '.' . $fileExtension;

            $request->file('photo')->storeAs('/public/images', $image);
        } else {
            $image = 'default.jpg';
        }

        $post = new Post();

        $post->photo = $image;
        $post->description = $request->description;
        $post->vote_count = 0;
        $post->user_id = Auth::user()->id;

        $post->save();

        return redirect('/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $post->description = $request->description;

        $post->save();

        return redirect('/posts/'.$post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post = Post::findOrFail($post->id);

        $vote = Vote::where('user_id', '=', Auth::user()->id)->where('post_id', '=', $post->id);

        $post->vote_count -= 1;

        $vote->delete();

        $post->save();

        return back();
    }

    public function user(int $id) {
        $posts = Post::where('user_id', '=', $id)->orderBy('created_at', 'Desc')->paginate(10);

        return view('posts.index', compact('posts'));
    }

    public function mostVotedPosts() {
        $posts = Post::orderBy('vote_count', 'Desc')->where('user_id', '=', Auth::user()->id)->paginate(10);

        return view('posts.mostvoted', compact('posts'));
    }
}
