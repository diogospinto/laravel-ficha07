<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function checkIfUserVoted() 
    {
        $votes = Vote::where('user_id', '=', Auth::user()->id)->where('post_id', '=', $this->id)->get();

        return ($votes->count() > 0) ? true : false;
    }
}
