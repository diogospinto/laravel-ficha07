<div class="container">

    <h2>Hi {{$post->user->name}}! </h2>

    <h3>Your photo received a new vote! </h3>

    <hr>

    <h4>Photo description: </h4>
    <p>{{$post->description}}</p>

    <hr>

    <small><i>Date: {{date('d-m-Y H:i')}}</i></small>

</div>