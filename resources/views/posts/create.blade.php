@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <form action="/posts" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="photo">Photo</label>
                        <input type="file" name="photo" class="form-control-file">
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" cols="30" rows="3" style="resize:none" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Create" class="btn btn-success btn-sm">
                    </div>

                </form>


            </div>
        </div>
    </div>

@endsection