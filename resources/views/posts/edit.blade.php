@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card text-center">

                    <form action="/posts/{{$post->id}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
    
                        <div class="card-header float-left">
    
                            <h6>{{$post->user->name}}</h6>
    
                            <img class="img-thumbnail" src="/images/{{$post->photo}}" alt="{{$post->description}}">
    
                        </div>
    
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" cols="30" rows="3" style="resize:none" class="form-control">{{$post->description}}</textarea>
                        </div>
    
                        <div class="form-group">
                            <input type="submit" value="Edit" class="btn btn-success btn-sm">
                        </div>
    
                    </form>

                </div>

            </div>
        </div>
    </div>

@endsection