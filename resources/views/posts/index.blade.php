@extends('layouts.app')

@section('content')
    
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">

                @include('layouts._flash')

                <div class="card">

                    <div class="card-header">

                        Posts

                        @if (count($posts) == 0)
                            
                            <div class="card">
                                <div class="card-header float-left">

                                    <div class="float-left">
                                        <h5>No posts created yet!</h5>
                                    </div>
                                     
                                </div>
                            </div>

                        @else
                            
                            @foreach ($posts as $post)
                                
                                <div class="card">
                                    <div class="card-header float-left">

                                        <div class="float-left">

                                            <a href="/users/{{$post->user->id}}/posts">{{$post->user->name}}</a>

                                        </div>

                                        <a href="/posts/{{$post->id}}"><img src="/images/{{$post->photo}}" alt="{{$post->user->name}}'s Photo'" class="img-thumbnail"></a>

                                        <div class="card-text float-left">

                                            {{($post->vote_count == 1) ? $post->vote_count . ' Vote' : (($post->vote_count == 0) ? 'No votes yet!' : $post->vote_count . ' Votes')}}

                                        </div>

                                        <br>

                                        <div class="card-subtitle text-muted float-right">
                                            <small><i>Created: {{$post->created_at->diffForHumans()}}</i></small>
                                        </div>

                                        @if ($post->user->id != Auth::user()->id)

                                            <br>
                                            <br>
                                        
                                            <div class="float-left">

                                                <form action="/posts/{{$post->id}}/upvote" method="post">
                                                    @csrf
    
                                                    <input type="submit" class="btn btn-success btn-sm" value="Upvote" {{($post->checkIfUserVoted()) ? 'disabled' : ''}}>
    
                                                </form>
                                            </div>

                                            <div class="float-left">

                                                <form action="/posts/{{$post->id}}" method="post">
                                                    @csrf
                                                    @method('DELETE')

                                                    <input type="submit" class="btn btn-danger btn-sm" value="Downvote" style="margin-left: 5px" {{$post->checkIfUserVoted() ? '' : 'disabled'}}>
                                                    
                                                </form>
                                            </div>
                                            
                                        @endif
                                        
                                    </div>
                                </div>

                            @endforeach

                        @endif

                    </div>

                </div>

                {{$posts->links()}}

            </div>
        </div>
    </div>

@endsection