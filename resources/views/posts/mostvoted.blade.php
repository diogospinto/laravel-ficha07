@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @if (count($posts) == 0)

                    <h6 class="text-center">No posts yet! </h6>
                    
                @else
                    
                @foreach ($posts as $post)
                    
                    <div class="card text-center">
                        <div class="card-header float-left">

                            <a href="/users/{{$post->user->id}}/posts"><h6>{{$post->user->name}}</h6></a>

                            <a href="/posts/{{$post->id}}"><img src="/images/{{$post->photo}}" alt="{{$post->description}}" class="img-thumbnail"></a>

                            <h6 class="card-title">{{$post->description}}</h6>

                            <div class="card-text">

                                {{($post->vote_count == 1) ? $post->vote_count . ' Vote' : (($post->vote_count == 0) ? 'No votes yet! ' : $post->vote_count . ' Votes')}}

                            </div>

                            <br>

                            <div class="card-subtitle mb-2 text-muted"><small><i>Created: {{$post->created_at->diffForHumans()}}</i></small></div>

                        </div>
                    </div>

                @endforeach

                @endif

                {{$posts->links()}}

            </div>
        </div>
    </div>

@endsection