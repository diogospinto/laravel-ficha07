@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card text-center">
                    <div class="card-header float-left">

                            <a href="/users/{{$post->user->id}}/posts"><h6>{{$post->user->name}}</h6></a>

                            <img src="/images/{{$post->photo}}" alt="{{$post->description}}" class="img-thumbnail">
    
                        <div class="card-title">
                            {{$post->description}}
                        </div>

                        <div class="card-text">

                            {{($post->vote_count == 1) ? $post->vote_count . ' Vote' : (($post->vote_count == 0) ? 'No votes yet!' : $post->vote_count . ' Votes')}}

                        </div>

                        <br>
    
                        <div class="card-subtitle mb-2 text-muted">
                            <small><i>Created: {{$post->created_at->diffForHumans()}}</i></small>
                        </div>

                        @if ($post->user_id == Auth::user()->id)
                            <div class="float-right">

                                <a href="/posts/{{$post->id}}/edit" class="btn btn-outline-primary btn-sm">Edit</a>
    
                            </div>

                        @else

                            <div class="float-left">

                                <form action="/posts/{{$post->id}}/upvote" method="post">
                                    @csrf

                                    <input type="submit" class="btn btn-success btn-sm" value="Upvote" {{($post->checkIfUserVoted()) ? 'disabled' : ''}}>

                                </form>
                            </div>

                            <div class="float-left">

                                <form action="/posts/{{$post->id}}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <input type="submit" class="btn btn-danger btn-sm" value="Downvote" style="margin-left: 5px" {{$post->checkIfUserVoted() ? '' : 'disabled'}}>
                                    
                                </form>
                            </div>

                        @endif
                        
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection